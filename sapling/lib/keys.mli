(** Spending key *)
type spending_key

(** Generate a spending key. 4.2.2 *)
val generate_spending_key : unit -> spending_key
