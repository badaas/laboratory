type spending_key = Bytes.t

let generate_spending_key () = Bigstring.to_bytes (Hacl.Rand.gen 32)
