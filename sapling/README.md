Sapling primitives experiments
=====================================

**It may never be something ready. That's an evening experiment at the moment,
while reading the sapling paper. But as I encourage OSS and open education then
I publish any piece of code I write as often as I can. Let me know if you want
to see more**

## Experiment introduction

The idea of this experiment is to implement sapling primitives with, as often as
possible, formally verified cryptographic primitives and math libraries. The
reference document is [ZCash Protocol Specification Version
2020.1.4](https://github.com/zcash/zips/blob/e4d9d2cace0e5bf246b3aa256f024011e8523bfe/protocol/sapling.pdf),
commit e4d9d2cace0e5bf246b3aa256f024011e8523bfe. A copy is attached in this git
repository.

The experiment is not focused on providing the fastest implementation, but the
most generic implementation, where the different hash functions or groups or
fields can be changed easily.


## Setup

```
opam switch create . 4.09.1
opam install --deps-only -y .
opam install merlin utop ocamlformat.0.10
```


## Play with utop

```
let spending_key = Sapling.generation_spending_key ();;
```
