#include "gmp.h"
#include "libff/algebra/fields/fp.hpp"
#include <cstdint>
#include <cstdio>
#include <vector>
#include "libfqfft/polynomial_arithmetic/basic_operations.hpp"

// We will use Fr from BLS12-381
typedef libff::bigint<4> bigint_fr;
const bigint_fr modulus = bigint_fr("524358751751261904794477405081859658376905"
                                    "52500527637822603658699938581184513");
typedef libff::Fp_model<4, modulus> Fr;

// Print a polynomial in a human readable format. The representation must contain the zero'es
template <typename FieldT> void print_polynomial(const std::vector<FieldT> &p) {
  for (int i = 0; i < p.size(); i++) {
    // We reduce the coefficient, see FieldT::print. Useful to use gmp_printf and have control over the printing.
    FieldT coef = p[p.size() - i - 1];
    FieldT tmp;
    tmp.mont_repr.data[0] = 1;
    tmp.mul_reduce(coef.mont_repr);

    // The coefficients are given as [a_0, a_1, ..., a_n]. We want to print a_n
    // X^n + ... + a_1 X + a_0.
    if (i == p.size() - 1) {
      gmp_printf("%Nd\n", tmp.mont_repr.data, FieldT::num_limbs);
    } else if (i == p.size() - 2) {
      gmp_printf("%Nd X + ", tmp.mont_repr.data, FieldT::num_limbs, p.size() - i - 1);
    } else {
      gmp_printf("%Nd X^%d + ", tmp.mont_repr.data, FieldT::num_limbs, p.size() - i - 1);
    }
  }
}


// Generate a random polynomial of the requested degree
template<typename FieldT>
std::vector<FieldT> generate_random_polynomial_of_degree(size_t degree) {
  std::vector<FieldT> v;
  for (auto i = 0; i < degree ; i++) {
    FieldT r = FieldT::random_element();
    v.push_back(r);
  }
  FieldT dominant_coefficient = FieldT::random_element();
  while (dominant_coefficient.is_zero()) {
    dominant_coefficient = FieldT::random_element();
  }
  v.push_back(dominant_coefficient);
  return (v);
}

int main() {
  Fr::Rsquared =
      bigint_fr("32949064747942654421297975206307107392785756821998006"
                "81788903916070560242797");
  Fr::Rcubed =
      bigint_fr("4982925398854031935455074224927608446012744635531591508"
                "9527227471280320770991");
  Fr::inv = 0xfffffffeffffffff;
  // Only for FFT
  Fr::root_of_unity = bigint_fr("2158412488654876019034639286702883068891255663"
                                "1271990304491841940743921295609");
  Fr::s = 32;
  // // std<vector> is used to represent the polynomials.
  // // The coefficients of the polynomials are given with the lower degree
  // // first. For instance, 2X^2 + X + 10 est représenté par [10, 1, 2] Let's
  // // try the is_zero function on the zero_polynomial.
  // const std::vector<Fr> zero_polynomial = {Fr::zero()};
  // std::cout << libfqfft::_is_zero<Fr>(zero_polynomial) << std::endl;

  // // Now with a random element.
  // Fr random_element = Fr::random_element();
  // random_element.print();
  // const std::vector<Fr> random_constant_polynomial = { random_element };
  // std::cout << libfqfft::_is_zero<Fr>(random_constant_polynomial) << std::endl;

  // // We will add two polynomials. The library libfqfft uses the concept of
  // // buffers: the result of an operation will be copied in the first parameter.
  // // Let's generate a random constant polynomial and check the functions
  // // _is_zero on the difference (computed using _polynomial_subtraction).
  // const std::vector<Fr> a = { random_element };
  // const std::vector<Fr> b = { random_element };
  // std::vector<Fr> diff_a_b;
  // libfqfft::_polynomial_subtraction(diff_a_b, a, b);
  // std::cout << libfqfft::_is_zero<Fr>(diff_a_b) << std::endl;

  // // If we generate two random constant polynomials, there is a high probability they are not equal:
  // const std::vector<Fr> a2 = { Fr::random_element() };
  // const std::vector<Fr> b2 = { Fr::random_element() };
  // std::vector<Fr> diff_a2_b2;
  // libfqfft::_polynomial_subtraction(diff_a2_b2, a2, b2);
  // std::cout << libfqfft::_is_zero<Fr>(diff_a2_b2) << std::endl;

  // const std::vector<Fr> random_polynomials = generate_random_polynomial_of_degree<Fr>(5);
  // print_polynomial<Fr>(random_polynomials);

  const std::vector<Fr> r1 =
      generate_random_polynomial_of_degree<Fr>(6);
  std::vector<Fr> r2 = generate_random_polynomial_of_degree<Fr>(6);
  std::vector<Fr> res;
  print_polynomial(r1);
  print_polynomial(r2);

  libfqfft::_polynomial_multiplication_on_fft(res, r1, r2);
  print_polynomial(res);

  return (0);
}
