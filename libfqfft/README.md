## Play with libfqfft

[libfqfft](https://github.com/scipr-lab/libfqfft)
We will compile from sources the libraries and the dependencies.
Directories in `$HOME` will be used to install the dependencies (headers +
library files). In this example, we will use `$HOME/.lib`. If you decide to
change this location, you must change the commands accordingly.

### Compile the dependencies

- libff

```
mkdir -p $HOME/.lib
git clone https://github.com/scipr-lab/libff /tmp/libff
cd /tmp/libff
git submodule init && git submodule update
mkdir build && cd build && cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/.lib/libff
make && make install
```

- googletest

```
cd /tmp
wget https://github.com/google/googletest/archive/release-1.10.0.zip
unzip release-1.10.0.zip
cd googletest-release-1.10.0
cmake . -DCMAKE_INSTALL_PREFIX=$HOME/.lib/googletest
make && make install
```

### Play with libfqfft and compile test files

First install libfqfft:
```
git clone https://github.com/scipr-lab/libfqfft /tmp/libfqfft
cd /tmp/libfqfft
git submodule init && git submodule update
mkdir build && cd build && cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/.lib/libfqfft
make & make install
```

To compile the tests in `libfqfft/tests`, use
```
g++ \
  -std=c++17 \
  -I$HOME/.lib/libff/include \
  -I$HOME/.lib/googletest/include/ -I$HOME/.lib/libfqfft/include -L$HOME/.lib/libff/lib -L$HOME/.lib/googletest/lib -L$HOME/.lib/libfqfft/lib evaluation_domain_test.cpp kronecker_substitution_test.cpp polynomial_arithmetic_test.cpp init_test.cpp -lgmp -lgtest -lpthread -lff -lgtest_main -lprocps -o libfqfft_test
```

and execute `./libfqfft_test` to run the test.

## Tutorials files

Some example files are given in `tutorials`, like `lagrange_polynomial_evaluation`.
To compile it, you can use the following command:
```
g++ \
  -std=c++17 \
  -I$HOME/.lib/libff/include \
  -I$HOME/.lib/libfqfft/include \
  -L$HOME/.lib/libff/lib \
  -L$HOME/.lib/libfqfft/lib \
  lagrange_polynomial_evaluation_example.cpp \
  -lgmp \
  -lpthread \
  -lff
  -o lagrange_polynomial_evaluation_example
```
and execute it with `./lagrange_polynomial_evaluation_example`

## Arithmetic over polynomials

Let's use some arithmetic functions with some finite fields we will define. Have
a look at `../libff/README.md` first to see how to use `libff` and define the
finite field you want. See `arithmetic.cpp` for some basic arithmetic operations
and some addons functions you may need.

To compile, use
```
g++ \
  -std=c++17 \
  -I$HOME/.lib/libff/include \
  -I$HOME/.lib/libfqfft/include \
  -L$HOME/.lib/libff/lib \
  -L$HOME/.lib/libfqfft/lib \
  arithmetic.cpp \
  -lgmp \
  -lpthread \
  -lff \
  -o arithmetic
```

## FFT

Two more parameters have to be defined:
- `FieldT::root_of_unity`: a `2**s`th root of unity.
- `FieldT::s`. Must be at least `logn` where `n` is the degree of the resulting polynomial (`n1 + n2` if polynomial multiplication)

The degree does not have to be a power of 2. For multiplication, the degrees can be different.

```
#include "gmp.h"
#include "libff/algebra/fields/fp.hpp"
#include <cstdint>
#include <cstdio>
#include <vector>
#include "libfqfft/polynomial_arithmetic/basic_operations.hpp"

// We will use Fr from BLS12-381
typedef libff::bigint<4> bigint_fr;
const bigint_fr modulus = bigint_fr("524358751751261904794477405081859658376905"
                                    "52500527637822603658699938581184513");
typedef libff::Fp_model<4, modulus> Fr;


int main() {
  Fr::Rsquared =
      bigint_fr("32949064747942654421297975206307107392785756821998006"
                "81788903916070560242797");
  Fr::Rcubed =
      bigint_fr("4982925398854031935455074224927608446012744635531591508"
                "9527227471280320770991");
  Fr::inv = 0xfffffffeffffffff;
  // For FFT
  Fr::root_of_unity = bigint_fr("2158412488654876019034639286702883068891255663"
                                "1271990304491841940743921295609");
  Fr::s = 32;

  const std::vector<Fr> r1 =
      generate_random_polynomial_of_degree<Fr>(6);
  std::vector<Fr> r2 = generate_random_polynomial_of_degree<Fr>(6);
  std::vector<Fr> res;
  print_polynomial(r1);
  print_polynomial(r2);

  libfqfft::_polynomial_multiplication_on_fft(res, r1, r2);
  print_polynomial(res);

  return (0);
}

```
