# Play with finite fields with libff

https://github.com/scipr-lab/libff/

## Compile

Supposing you compiled libff and install it in `$HOME/.lib/libff`, use
```
g++ -std=c++17 -I$HOME/.lib/libff/include -L$HOME/.lib/libff/lib bls12_381_fr.cpp -lgmp
```

## Parameters
libff requires a Montgomery representation, and the user has to give the parameters R, R^2, R^3 and mu.

```ocaml
let compute_libff_parameters word_size modulus =
  let exp = Z.pow (Z.of_string "2") word_size in
  let rsquared = Z.erem (Z.pow exp 2) modulus in
  let rcube = Z.erem (Z.pow exp 3) modulus in
  let exp = Z.pow (Z.of_string "2") 64 in
  let inv = Z.erem (Z.neg (Z.invert modulus exp)) exp in
  Printf.printf "R = %s, Rsquared = %s, Rcube = %s, inv = %s (hex = %s)" (Z.to_string exp) (Z.to_string rsquared) (Z.to_string rcube) (Z.to_string inv) (Hex.show (Hex.of_bytes (Bytes.of_string (Z.to_bits inv))));;
```

## Links

- [Montgomery Arithmetic from a Software Perspective](https://eprint.iacr.org/2017/1057.pdf)
