#include <cstdint>
#include <cstdio>
#include "libff/algebra/fields/fp.hpp"
#include "gmp.h"

using namespace libff;

typedef bigint<4> bigint_fr;
const bigint_fr modulus = bigint_fr("52435875175126190479447740508185965837690552500527637822603658699938581184513");
typedef Fp_model<4, modulus> FR;

int main() {
  // Init parameters
  // R = 2^64
  // R^2
  FR::Rsquared = bigint_fr("3294906474794265442129797520630710739278575682199800681788903916070560242797");
  // R^3
  FR::Rcubed = bigint_fr("49829253988540319354550742249276084460127446355315915089527227471280320770991");
  // -modulus^(-1) mod R
  FR::inv = 0xfffffffeffffffff;
  // ------
  // ----
  FR a = FR::random_element();
  FR inverse_a = a.inverse();
  a.print();
  FR b = FR::random_element();
  FR inverse_b = b.inverse();
  b.print();
  FR result_inverses = inverse_b * inverse_a;
  FR mult_result = a * b;
  (b * inverse_b).print();
  std::cout << "Inverse of product: ";
  mult_result.inverse().print();
  std::cout << "Product of inverses: " << std::endl;
  result_inverses.print();
  std::cout << "\n";
  return (0);
}
